﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    public class Operacje : IOperacje
    {
        IPrad prad;
        IWoda woda;
        ISzczotki szczotki;
        
        public Operacje()
        {
            prad = new Prad();
            woda = new Woda();
            szczotki = new Szczotki();
        }

        public void Start()
        {
            //throw new NotImplementedException();
            prad.wlacz();
            woda.Plyn();
            szczotki.uruchom();
        }

        public void Stop()
        {
            //throw new NotImplementedException();
            prad.wylacz();
            woda.Nieplyn();
            szczotki.zatrzymaj();
        }
        public IPrad Prad()
        {
            return this.prad;
        }
        public IWoda Woda()
        {
            return this.woda;
        }
        public ISzczotki Szczotki()
        {
            return this.szczotki;
        }

        public static object GetPrad(object prad)
        {
            return (prad as Operacje).Prad();
        }
        public static object GetWoda(object woda)
        {
            return (woda as Operacje).Woda();
        }
        public static object GetSzczotki(object szczotki)
        {
            return (szczotki as Operacje).Szczotki();
        }
    }
}
