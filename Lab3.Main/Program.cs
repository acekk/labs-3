﻿using System;
using Lab3.Implementation;
using Lab3.Contract;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Operacje operacja = new Operacje();
            operacja.Start();
            operacja.Stop();
        }
    }
}
