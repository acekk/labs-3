﻿using System;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(IPrad);
        public static Type I2 = typeof(IWoda);
        public static Type I3 = typeof(ISzczotki);

        public static Type Component = typeof(Operacje);

        public static GetInstance GetInstanceOfI1 = Operacje.GetPrad;
        public static GetInstance GetInstanceOfI2 = Operacje.GetWoda;
        public static GetInstance GetInstanceOfI3 = Operacje.GetSzczotki;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(ExtraOperacje);
        public static Type MixinFor = typeof(Operacje);

        #endregion
    }
}
